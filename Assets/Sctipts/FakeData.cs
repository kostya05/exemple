﻿using BetaWorld.UnityBundleSystem;
using Newtonsoft.Json.Linq;
using Sctipts.UI.DrawHolders;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Sctipts
{
	public static class FakeData
	{
		private static readonly JArray ValueFakeData;
		private static readonly JArray ValueWithIconFakeData;

		static FakeData()
		{
			ValueFakeData = new JArray();
			for (int i = 0; i < 50; i++)
			{
				ValueFakeData.Add(new JObject
				{
					{ "type", TextHolder.LinkType },
					{ "text", "Случайное число: " + Random.Range(0, 1000) }
				});
			}

			var texts = Resources.Load<TextAsset>("FirstNames");
			var firstNames = Resources.Load<TextAsset>("FirstNames").text.Split('\n');
			var lastNames = Resources.Load<TextAsset>("LastNames").text.Split('\n');

			for (int i = 0; i < 200; i++)
			{
				ValueFakeData.Add(new JObject
				{
					{ "type", TextHolder.LinkType },
					{ "text", firstNames[Random.Range(0, firstNames.Length)] + ' ' + lastNames[Random.Range(0, lastNames.Length)] }
				});
			}
			ValueWithIconFakeData = new JArray();
			for (int i = 0; i < 50; i++)
			{
				ValueWithIconFakeData.Add(new JObject()
				{
					{ "type", IconWithTextHolder.LinkType },
					{ "icon", JToken.FromObject(new CategoryObject("Icons", Random.Range(0, 5).ToString())) },
					{ "text", ValueFakeData[Random.Range(0, ValueFakeData.Count)]["text"] }
				});
			}
		}

		public static JToken GetElement()
		{
			var value = Random.value;
			if (value > 0.5f)
				return ValueWithIconFakeData[Random.Range(0, ValueWithIconFakeData.Count)];
			return ValueFakeData[Random.Range(0, ValueFakeData.Count)];
		}
	}
}
