﻿using System;
using UnityEngine;

namespace Sctipts.Core
{
	public abstract class Singleton<T> : MonoBehaviour where T : Singleton<T>
	{
		// ReSharper disable once StaticMemberInGenericType
		private static readonly object SingletonInstanceLock = new object();

		private static T _instance;
		protected static T Instance
		{
			get
			{
				lock (SingletonInstanceLock)
				{
					if (_instance == null)
						_instance = FindObjectOfType<T>();
					if (_instance == null)
						_instance = new GameObject(typeof(T).Name, typeof(T)).GetComponent<T>();
					return _instance;
				}
			}
		}

		public static bool IsInitializee { get { return Instance != null && !_instance.Equals(null); } }
		public static bool IsInitializeeComplete { get { return IsInitializee && _instance._isInitializeeComplet; } }

		public bool IsInstanceInitializee { get { return !Equals(null) && _isInitializeeComplet; } }

		public event Action Destroyed; //TODO: Подумать в сторону UnityEvent для предотращения десериализации

		private event Action<T> InitializeeCompleted;//TODO: Подумать в сторону UnityEvent для предотращения десериализации

		[SerializeField, HideInInspector]
		private bool _isInitializeeComplet;
		
		private void Awake()
		{
			if (_instance == null)
			{
				_instance = (T)this;
				DontDestroyOnLoad(gameObject);
				lock (SingletonInstanceLock)
				{
					_isInitializeeComplet = true;
					Initializee();
					if(InitializeeCompleted != null)
						InitializeeCompleted.Invoke(_instance);
				}
			}
			else if (this != _instance)
			{
				DestroyImmediate(gameObject);
			}
		}

		private void OnDestroy()
		{
			if (Destroyed != null)
				Destroyed.Invoke();
		}

		protected abstract void Initializee();

		public static void Execute(Action<T> action)
		{
			lock (SingletonInstanceLock)
			{
				if (IsInitializeeComplete)
					action.Invoke(_instance);
				else
					_instance.InitializeeCompleted += action;
			}
		}

		public static void CancelExecute(Action<T> action)
		{
			lock (SingletonInstanceLock)
			{
				_instance.InitializeeCompleted -= action;
			}
		}
	}
}
