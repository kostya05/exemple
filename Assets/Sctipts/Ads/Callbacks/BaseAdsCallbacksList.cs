﻿using System.Collections.Generic;

namespace Sctipts.Ads.Callbacks
{
	public abstract class BaseAdsCallbacksList
	{
		public enum TypeCallback
		{
			Loaded,
			FailedToLoad,
			Shown,
			Finished,
			Closed,
			Clicked
		}

		protected Dictionary<TypeCallback, IAdsCallback> Callbacks;

		public bool HaveCallback(TypeCallback type)
		{
			return Callbacks.ContainsKey(type);
		}

		public IAdsCallback this[TypeCallback type]
		{
			get
			{
				if (HaveCallback(type))
					return Callbacks[type];
				return null;
			}
		}

		protected void Invoke(TypeCallback type, params object[] args)
		{
			((IAdsCallbackInvoke)Callbacks[type]).Invoke(args);
		}

		protected void Invoke(TypeCallback type)
		{
			((IAdsCallbackInvoke)Callbacks[type]).Invoke(null);
		}
	}
}
