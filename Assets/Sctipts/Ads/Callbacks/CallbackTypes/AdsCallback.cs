﻿using System;

namespace Sctipts.Ads.Callbacks.CallbackTypes
{
	public sealed class AdsCallback : IAdsCallback, IAdsCallbackInvoke
	{
		private event Action _actions;

		public bool HaveArgs { get { return false; } }

		public void AddLisenter(Action action)
		{
			_actions += action;
		}

		public void RemoveLisenter(Action action)
		{
			_actions -= action;
		}

		public void AddLisenter(Action<object[]> action)
		{
			throw new NotImplementedException();
		}

		public void RemoveLisenter(Action<object[]> action)
		{
			throw new NotImplementedException();
		}

		void IAdsCallbackInvoke.Invoke(object[] args)
		{
			if (_actions != null)
				_actions.Invoke();
		}
	}
}