﻿using System;

namespace Sctipts.Ads.Callbacks.CallbackTypes
{
	public sealed class ArgsAdsCallback : IAdsCallback, IAdsCallbackInvoke
	{
		private event Action<object[]> _actions;

		public bool HaveArgs { get { return true; } }
		public void AddLisenter(Action action)
		{
			throw new NotImplementedException();
		}

		public void RemoveLisenter(Action action)
		{
			throw new NotImplementedException();
		}

		public void AddLisenter(Action<object[]> action)
		{
			_actions += action;
		}

		public void RemoveLisenter(Action<object[]> action)
		{
			_actions -= action;
		}

		void IAdsCallbackInvoke.Invoke(object[] args)
		{
			if (_actions != null)
				_actions.Invoke(args);
		}

	}
}
