﻿using System.Collections.Generic;
using AppodealAds.Unity.Api;
using AppodealAds.Unity.Common;
using Sctipts.Ads.Callbacks.CallbackTypes;

namespace Sctipts.Ads.Callbacks.CustomCallbacks
{
	public sealed class BannerCallbacks : BaseAdsCallbacksList, IBannerAdListener
	{
		public BannerCallbacks()
		{
			Callbacks = new Dictionary<TypeCallback, IAdsCallback>
			{
				{ TypeCallback.Loaded, new AdsCallback() },
				{ TypeCallback.FailedToLoad, new AdsCallback() },
				{ TypeCallback.Shown, new AdsCallback() },
				{ TypeCallback.Clicked, new AdsCallback() }
			};
			Appodeal.setBannerCallbacks(this);
		}

		public void onBannerLoaded()
		{
			Invoke(TypeCallback.Loaded);
		}

		public void onBannerFailedToLoad()
		{
			Invoke(TypeCallback.FailedToLoad);
		}

		public void onBannerShown()
		{
			Invoke(TypeCallback.Shown);
		}

		public void onBannerClicked()
		{
			Invoke(TypeCallback.Clicked);
		}
	}
}
