﻿using System.Collections.Generic;
using AppodealAds.Unity.Api;
using AppodealAds.Unity.Common;
using Sctipts.Ads.Callbacks.CallbackTypes;

namespace Sctipts.Ads.Callbacks.CustomCallbacks
{
	public sealed class InterstitialCallbacks : BaseAdsCallbacksList, IInterstitialAdListener
	{
		public InterstitialCallbacks()
		{
			Callbacks = new Dictionary<TypeCallback, IAdsCallback>
			{
				{ TypeCallback.Loaded, new AdsCallback() },
				{ TypeCallback.FailedToLoad, new AdsCallback() },
				{ TypeCallback.Shown, new AdsCallback() },
				{ TypeCallback.Clicked, new AdsCallback() },
				{ TypeCallback.Closed, new AdsCallback() }
			};
			Appodeal.setInterstitialCallbacks(this);
		}

		public void onInterstitialLoaded()
		{
			Invoke(TypeCallback.Loaded);
		}

		public void onInterstitialFailedToLoad()
		{
			Invoke(TypeCallback.FailedToLoad);
		}

		public void onInterstitialShown()
		{
			Invoke(TypeCallback.Shown);
		}

		public void onInterstitialClosed()
		{
			Invoke(TypeCallback.Closed);
		}

		public void onInterstitialClicked()
		{
			Invoke(TypeCallback.Clicked);
		}
	}
}
