﻿using System.Collections.Generic;
using AppodealAds.Unity.Api;
using AppodealAds.Unity.Common;
using Sctipts.Ads.Callbacks.CallbackTypes;

namespace Sctipts.Ads.Callbacks.CustomCallbacks
{
	public sealed class SkippableVideoCallbacks : BaseAdsCallbacksList, ISkippableVideoAdListener
	{
		public SkippableVideoCallbacks()
		{
			Callbacks = new Dictionary<TypeCallback, IAdsCallback>
			{
				{ TypeCallback.Loaded, new AdsCallback() },
				{ TypeCallback.FailedToLoad, new AdsCallback() },
				{ TypeCallback.Shown, new AdsCallback() },
				{ TypeCallback.Finished, new AdsCallback() },
				{ TypeCallback.Closed, new AdsCallback() }
			};
			Appodeal.setSkippableVideoCallbacks(this);
		}

		public void onSkippableVideoLoaded()
		{
			Invoke(TypeCallback.Loaded);
		}

		public void onSkippableVideoFailedToLoad()
		{
			Invoke(TypeCallback.FailedToLoad);
		}

		public void onSkippableVideoShown()
		{
			Invoke(TypeCallback.Shown);
		}

		public void onSkippableVideoFinished()
		{
			Invoke(TypeCallback.Finished);
		}

		public void onSkippableVideoClosed()
		{
			Invoke(TypeCallback.Closed);
		}
	}
}
