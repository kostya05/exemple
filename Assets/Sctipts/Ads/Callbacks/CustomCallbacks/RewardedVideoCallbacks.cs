﻿using System.Collections.Generic;
using AppodealAds.Unity.Api;
using AppodealAds.Unity.Common;
using Sctipts.Ads.Callbacks.CallbackTypes;

namespace Sctipts.Ads.Callbacks.CustomCallbacks
{
	public sealed class RewardedVideoCallbacks : BaseAdsCallbacksList, IRewardedVideoAdListener
	{
		public RewardedVideoCallbacks()
		{
			Callbacks = new Dictionary<TypeCallback, IAdsCallback>
			{
				{ TypeCallback.Loaded, new AdsCallback() },
				{ TypeCallback.FailedToLoad, new AdsCallback() },
				{ TypeCallback.Shown, new AdsCallback() },
				{ TypeCallback.Finished, new ArgsAdsCallback() },
				{ TypeCallback.Closed, new AdsCallback() }
			};
			Appodeal.setRewardedVideoCallbacks(this);
		}

		public void onRewardedVideoLoaded()
		{
			Invoke(TypeCallback.Loaded);
		}

		public void onRewardedVideoFailedToLoad()
		{
			Invoke(TypeCallback.FailedToLoad);
		}

		public void onRewardedVideoShown()
		{
			Invoke(TypeCallback.Shown);
		}

		public void onRewardedVideoFinished(int amount, string name)
		{
			Invoke(TypeCallback.Finished, amount, name);
		}

		public void onRewardedVideoClosed()
		{
			Invoke(TypeCallback.Closed);
		}
	}
}
