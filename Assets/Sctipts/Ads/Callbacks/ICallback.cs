﻿using System;

namespace Sctipts.Ads.Callbacks
{
	public interface IAdsCallback
	{
		bool HaveArgs { get; }

		void AddLisenter(Action action);
		void RemoveLisenter(Action action);

		void AddLisenter(Action<object[]> action);
		void RemoveLisenter(Action<object[]> action);
	}
}