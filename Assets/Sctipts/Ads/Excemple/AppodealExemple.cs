﻿using Sctipts.Ads.Callbacks;
using UnityEngine;
using UnityEngine.UI;

namespace Sctipts.Ads.Excemple
{
	public class AppodealExemple : MonoBehaviour
	{
		private AppodealManager _appodealManager;

		[SerializeField]
		private Button _btnAdsBanner;

		[SerializeField]
		private Button _customButton;
		[SerializeField]
		private AppodealManager.TypesAds _typeAdsCustomButton = AppodealManager.TypesAds.INTERSTITIAL;

		private bool IsAdsInit { get { return _appodealManager != null && _appodealManager.IsInstanceInitializee; } }

		#region Встроенные в юнити методы

		private void Start()
		{
			BindAppodealManager();
		}

		private void OnDestroy()
		{
			if (!IsAdsInit)
				return;

			_appodealManager.Destroyed -= BindAppodealManager;
			RemoveCallbacks(); //Отписываемся от событий в случае смерти AppodealExemple
		}

		private void Update()
		{
			if (!IsAdsInit)
				return;

			if (_btnAdsBanner != null)
				_btnAdsBanner.interactable = _appodealManager.IsLoadedAds(AppodealManager.TypesAds.BANNER);

			if(_customButton != null)
				_customButton.interactable = _appodealManager.IsLoadedAds(_typeAdsCustomButton);
		}

		#endregion

		#region Внутреняя логика

		private void Initialize(AppodealManager appodealManager)
		{
			_appodealManager = appodealManager;
			AddCallbacks(); //Подписываемся на события
		}

		private void BindAppodealManager()
		{
			if (_appodealManager != null)
				RemoveCallbacks(); //Отписываемся от событий в случае смерти старого _appodealManager
			AppodealManager.Execute(Initialize);
		}

		private void AddCallbacks()
		{
			_appodealManager.Destroyed += BindAppodealManager; //Если AppodealManager умрёт говорим что нужно запросить его заного.

			if (_appodealManager.SupportAdsCallbacks(AppodealManager.TypesAds.BANNER)) //Поддерживаются ли события от баннерной рекламмы.
			{
				var callbacks = _appodealManager.GetCallbacks(AppodealManager.TypesAds.BANNER); //Получаем события с баннерной рекламмы.

				if (callbacks.HaveCallback(BaseAdsCallbacksList.TypeCallback.Clicked)) //Проверяем поддерживает ли рекламма события, что по банеру кликнули
					callbacks[BaseAdsCallbacksList.TypeCallback.Clicked].AddLisenter(OnBannerAdsClicked); //Подписывемся на событие

				if (callbacks.HaveCallback(BaseAdsCallbacksList.TypeCallback.Shown)) //Проверяем поддерживает ли рекламма события, что по банер показался
					callbacks[BaseAdsCallbacksList.TypeCallback.Shown].AddLisenter(OnBannerShown); //Подписывемся на событие
			}

			if (_appodealManager.SupportAdsCallbacks(_typeAdsCustomButton)) //Поддерживаются ли события для _typeAdsCustomButton
			{
				var callbacks = _appodealManager.GetCallbacks(_typeAdsCustomButton); //Получаем события для _typeAdsCustomButton.

				if (callbacks.HaveCallback(BaseAdsCallbacksList.TypeCallback.Clicked)) //Проверяем поддерживает ли рекламма события, что кликнули
					callbacks[BaseAdsCallbacksList.TypeCallback.Clicked].AddLisenter(OnCustomAdsClicked); //Подписывемся на событие

				if (callbacks.HaveCallback(BaseAdsCallbacksList.TypeCallback.Shown)) //Проверяем поддерживает ли рекламма события, что по рекламма показалась
					callbacks[BaseAdsCallbacksList.TypeCallback.Shown].AddLisenter(OnCustomAdsShown); //Подписывемся на событие

				if (callbacks.HaveCallback(BaseAdsCallbacksList.TypeCallback.Finished)) //Проверяем поддерживает ли рекламма события, что по рекламма завершилась
					callbacks[BaseAdsCallbacksList.TypeCallback.Finished].AddLisenter(OnCustomAdsFinished); //Подписывемся на событие
			}
		}


		private void RemoveCallbacks()
		{
			if (_appodealManager.Equals(null)) //Проверка что объект не полностью умер. Если _appodealManager == null возвращает false это не говорит что объет жив. MonoBehaviour...
				return;
			_appodealManager.Destroyed -= BindAppodealManager;

			if (_appodealManager.SupportAdsCallbacks(AppodealManager.TypesAds.BANNER)) //Поддерживаются ли события от баннерной рекламмы.
			{
				var callbacks = _appodealManager.GetCallbacks(AppodealManager.TypesAds.BANNER); //Получаем события с баннерной рекламмы.

				if (callbacks.HaveCallback(BaseAdsCallbacksList.TypeCallback.Clicked)) //Проверяем поддерживает ли рекламма события, что по банеру кликнули
					callbacks[BaseAdsCallbacksList.TypeCallback.Clicked].RemoveLisenter(OnBannerAdsClicked); //Отписывемся от события

				if (callbacks.HaveCallback(BaseAdsCallbacksList.TypeCallback.Shown)) //Проверяем поддерживает ли рекламма события, что по банер показался
					callbacks[BaseAdsCallbacksList.TypeCallback.Shown].RemoveLisenter(OnBannerShown); //Отписывемся от события
			}

			if (_appodealManager.SupportAdsCallbacks(_typeAdsCustomButton)) //Поддерживаются ли события для _typeAdsCustomButton
			{
				var callbacks = _appodealManager.GetCallbacks(_typeAdsCustomButton); //Получаем события для _typeAdsCustomButton.

				if (callbacks.HaveCallback(BaseAdsCallbacksList.TypeCallback.Clicked)) //Проверяем поддерживает ли рекламма события, что кликнули
					callbacks[BaseAdsCallbacksList.TypeCallback.Clicked].RemoveLisenter(OnCustomAdsClicked); //Отписывемся от события

				if (callbacks.HaveCallback(BaseAdsCallbacksList.TypeCallback.Shown)) //Проверяем поддерживает ли рекламма события, что по рекламма показалась
					callbacks[BaseAdsCallbacksList.TypeCallback.Shown].RemoveLisenter(OnCustomAdsShown); //Отписывемся от события

				if (callbacks.HaveCallback(BaseAdsCallbacksList.TypeCallback.Finished)) //Проверяем поддерживает ли рекламма события, что по рекламма завершилась
					callbacks[BaseAdsCallbacksList.TypeCallback.Finished].RemoveLisenter(OnCustomAdsFinished); //Отписывемся от события
			}
		}

		#endregion

		#region Методы взаимодействия в рекламмой

		public void ShowBannerAds()
		{
			if (!IsAdsInit)
				return;

			_appodealManager.ShowAds(AppodealManager.TypesAds.BANNER);
		}

		public void ShowCustomAds()
		{
			if (!IsAdsInit)
				return;

			_appodealManager.ShowAds(_typeAdsCustomButton);
		}

		#endregion

		#region Callbacks

		private void OnBannerShown()
		{
			Debug.Log("Банер показался!");
		}

		private void OnBannerAdsClicked()
		{
			Debug.Log("По банеру кликнули!");
		}


		private void OnCustomAdsFinished()
		{
			Debug.Log("CustomAds завершился!");
		}

		private void OnCustomAdsShown()
		{
			Debug.Log("CustomAds показался!");
		}

		private void OnCustomAdsClicked()
		{
			Debug.Log("CustomAds Clicked!");
		}

		#endregion

	}
}
