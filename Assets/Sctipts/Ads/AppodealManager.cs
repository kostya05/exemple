﻿using System.Collections.Generic;
using AppodealAds.Unity.Api;
using Sctipts.Ads.Callbacks;
using Sctipts.Ads.Callbacks.CustomCallbacks;
using Sctipts.Core;
using UnityEngine;

namespace Sctipts.Ads
{
	public sealed class AppodealManager : Singleton<AppodealManager>
	{
		public enum TypesAds
		{
			// ReSharper disable InconsistentNaming
			INTERSTITIAL = Appodeal.INTERSTITIAL,
			SKIPPABLE_VIDEO = Appodeal.SKIPPABLE_VIDEO,
			BANNER = Appodeal.BANNER,
			REWARDED_VIDEO = Appodeal.REWARDED_VIDEO
			// ReSharper restore InconsistentNaming
		}

		[SerializeField]
		private string _appKey = "";

		private Dictionary<TypesAds, BaseAdsCallbacksList> _callbacks;

		protected override void Initializee()
		{
			Appodeal.disableLocationPermissionCheck();
			Appodeal.initialize(_appKey, Appodeal.INTERSTITIAL | Appodeal.SKIPPABLE_VIDEO | Appodeal.BANNER | Appodeal.REWARDED_VIDEO);
			_callbacks = new Dictionary<TypesAds, BaseAdsCallbacksList>
			{
				{ TypesAds.SKIPPABLE_VIDEO, new SkippableVideoCallbacks() },
				{ TypesAds.BANNER, new BannerCallbacks() },
				{ TypesAds.REWARDED_VIDEO, new RewardedVideoCallbacks() },
				{ TypesAds.INTERSTITIAL, new InterstitialCallbacks() }
			};
		}

		/// <summary>
		///
		/// </summary>
		/// <param name="adsType">Get to "Appodeal.ADS_TYPE"</param>
		public bool IsLoadedAds(TypesAds adsType)
		{
			return Appodeal.isLoaded((int)adsType);
		}

		public bool ShowAds(TypesAds adsType)
		{
			if(IsLoadedAds(adsType))
				return Appodeal.show((int)adsType);
			return false;
		}

		public bool SupportAdsCallbacks(TypesAds adsType)
		{
			return _callbacks.ContainsKey(adsType);
		}

		public BaseAdsCallbacksList GetCallbacks(TypesAds adsType)
		{
			if (_callbacks.ContainsKey(adsType))
				return _callbacks[adsType];
			return null;
		}
	}
}
