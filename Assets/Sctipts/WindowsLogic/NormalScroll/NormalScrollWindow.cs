﻿using BetaWorld.UnityBundleSystem;
using Sctipts.UI.DrawHolders;
using Sctipts.UI.Windows;
using UnityEngine;

namespace Sctipts.WindowsLogic.NormalScroll
{
	public class NormalScrollWindow : Window
	{
		[SerializeField]
		private Transform _content;
		[SerializeField]
		private string _selectCategory;
		
		protected override void ChangeActive(bool active)
		{
			if (active)
				OnActivate();
			else
			{
				for (int i = 0; i < _content.childCount; i++)
				{
					Destroy(_content.GetChild(i).gameObject);
				}
			}
		}

		private void OnActivate()
		{
			for (int i = 0; i < 1000; i++)
			{
				var model = FakeData.GetElement();
				string type = null;
				switch (model["type"].ToString())
				{
					case TextHolder.LinkType:
						type = "TextHolder";
						break;
					case IconWithTextHolder.LinkType:
						type = "IconWithTextHolder";
						break;
					default:
						continue;
				}
				var holder = BundleSystem.GetObject<GameObject>(new CategoryObject(_selectCategory, type));
				Instantiate(holder, _content).GetComponent<IDrawValue>().SetValue(model);
			}
		}

		public void SetCategort(string category)
		{
			_selectCategory = category;
		}
	}
}
