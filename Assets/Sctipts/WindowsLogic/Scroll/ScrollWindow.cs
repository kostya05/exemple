﻿using BetaWorld.InfiniteScroll;
using BetaWorld.Pool;
using BetaWorld.UnityBundleSystem;
using Sctipts.UI.Windows;
using UnityEngine;

namespace Sctipts.WindowsLogic.Scroll
{
	public class ScrollWindow : Window
	{
		[SerializeField]
		private InfiniteScrollRect _scroll;
		[SerializeField]
		private Transform _poolContnet;
		[SerializeField]
		private string _selectCategory;

		private ExempleScrollAdapter _adapter;

		protected override void GetCacheComponents()
		{
			base.GetCacheComponents();

			if (_scroll == null)
				_scroll = GetComponentInChildren<InfiniteScrollRect>();
			_scroll.SetPool(new UnityObjectsPool(_poolContnet));
		}

		protected override void ChangeActive(bool active)
		{
			if (active)
				OnActivate();
		}

		private void OnActivate()
		{
			if(_adapter == null)
				_adapter = new ExempleScrollAdapter(_selectCategory, _scroll, BundleSystem.GetObject<GameObject>);
			else
				_adapter.SetCategory(_selectCategory);
			
			SetFakeData();
			_scroll.SetAdapter(_adapter);
		}

		private void SetFakeData()
		{
			var data = _adapter.GetData();
			data.Clear();
			for (int i = 0; i < 1000; i++)
				data.Add(FakeData.GetElement());
			_adapter.NotifyAll();
		}

		public void SetCategort(string category)
		{
			_selectCategory = category;
		}
	}
}
