﻿using System;
using System.Collections.Generic;
using BetaWorld.InfiniteScroll;
using BetaWorld.UnityBundleSystem;
using JetBrains.Annotations;
using Newtonsoft.Json.Linq;
using Sctipts.UI.DrawHolders;
using UnityEngine;

namespace Sctipts.WindowsLogic.Scroll
{
	public class ExempleScrollAdapter : ScrollAdapter<JToken, CategoryObject>
	{
		private CategoryObject[] _allCategories;

		public ExempleScrollAdapter(string category, [NotNull] InfiniteScrollRect owner, [NotNull] Func<CategoryObject, GameObject> getObject) : base(owner, getObject)
		{
			SetCategory(category);
		}
		

		private Dictionary<int, Type> _typesInPostion;

		public void SetCategory(string category)
		{
			_allCategories = new[]
			{
				new CategoryObject(category, "TextHolder"), 
				new CategoryObject(category, "IconWithTextHolder") 
			};
			CreateTypesMap();
		}

		public override void NotifyAll()
		{
			BindPositions();
			base.NotifyAll();
		}

		private void BindPositions()
		{
			if (_typesInPostion == null)
				_typesInPostion = new Dictionary<int, Type>();
			else
				_typesInPostion.Clear();

			for (int i = 0; i < DataSet.Count; i++)
			{
				var element = DataSet[i];

				switch (element["type"].ToString())
				{
					case TextHolder.LinkType:
						_typesInPostion[i] = typeof(TextHolder);
						break;
					case IconWithTextHolder.LinkType:
						_typesInPostion[i] = typeof(IconWithTextHolder);
						break;
				}
			}
		}
		
		protected override CategoryObject[] AllElementCategories
		{
			get
			{
				if (_allCategories == null)
					return new CategoryObject[0];
				return _allCategories;
			}
		}

		public override object ElementType(int position)
		{
			var type = GetHolderType(position);
			if (type == typeof(TextHolder))
				return _allCategories[0];
			return _allCategories[1];
		}

		public override Type GetHolderType(int position)
		{
			return _typesInPostion[position];
		}

		public override void OnBindViewHolder(ViewHolder viewHolder, int position)
		{
			base.OnBindViewHolder(viewHolder, position);
			//var type = GetHolderType(position);
			var drawValue = viewHolder as IDrawValue;
			if(drawValue != null)
				drawValue.SetValue(DataSet[position]);
		}
	}
}
