﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Sctipts.UI.Windows
{
	[RequireComponent(typeof(Animator))]
	public class Window : MonoBehaviour
	{
		private static readonly Dictionary<int, Window> _windows = new Dictionary<int, Window>();
		private static Window _openWindows;

		[SerializeField]
		private Animator _animator;
		[SerializeField]
		private bool _isDef;
		[SerializeField]
		private UnityEvent _onForceActive;
		[SerializeField]
		private UnityEvent _onForceDisable;

		private void Awake()
		{
			_windows[GetInstanceID()] = this;

			GetCacheComponents();
			
			_onForceActive.AddListener(() => ChangeActive(true));
			_onForceDisable.AddListener(() => ChangeActive(false));
			
			if (_openWindows != null)
				_isDef = false;
			ForceWindowActive(_isDef);
		}

		protected virtual void GetCacheComponents()
		{
			_animator = GetComponent<Animator>();
		}

		private void ForceWindowActive(bool active)
		{
			if (active)
			{
				if (_openWindows != null)
					_openWindows._onForceDisable.Invoke();

				_openWindows = this;
				_onForceActive.Invoke();
			}
			else
			{
				if (_openWindows == this)
					return;
				_onForceDisable.Invoke();
			}
		}

		public void Show()
		{
			if (!_animator.enabled)
				_animator.enabled = true;

			if (_openWindows != null)
				_openWindows.Hide();
			_openWindows = this;
			_animator.SetTrigger("Show");
			ChangeActive(true);
		}

		private void Hide()
		{
			if (!_animator.enabled)
				_animator.enabled = true;

			_animator.SetTrigger("Hide");
			ChangeActive(false);
		}

		protected virtual void ChangeActive(bool active)
		{
			
		}
	}
}
