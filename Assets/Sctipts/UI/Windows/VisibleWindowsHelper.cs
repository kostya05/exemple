﻿using UnityEngine;

namespace Sctipts.UI.Windows
{
	[RequireComponent(typeof(CanvasGroup))]
	public class VisibleWindowsHelper : MonoBehaviour
	{
		[SerializeField]
		private CanvasGroup _canvasGroup;

		public CanvasGroup CanvasGroup
		{
			get
			{
				if (_canvasGroup == null)
					_canvasGroup = GetComponent<CanvasGroup>();
				return _canvasGroup;
			}
		}

		public void SetActive(bool active)
		{
			var canvasGroup = CanvasGroup;

			canvasGroup.alpha = active ? 1f : 0f;
			canvasGroup.interactable = _canvasGroup.blocksRaycasts = active;
		}
	}
}
