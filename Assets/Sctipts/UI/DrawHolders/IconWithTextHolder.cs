﻿using BetaWorld.UnityBundleSystem;
using Newtonsoft.Json.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace Sctipts.UI.DrawHolders
{
	public sealed class IconWithTextHolder : TextHolder
	{
		public const string LinkType = "IconWithText";

		[SerializeField]
		private Image _icon;

		public override void SetValue(JToken value)
		{
			var cartegoriIcon = value["icon"].ToObject<CategoryObject>();
			_icon.sprite = BundleSystem.GetObject<Sprite>(cartegoriIcon);
			_text.text = value["text"].ToString();
		}
	}
}
