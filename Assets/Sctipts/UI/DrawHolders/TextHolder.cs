﻿using BetaWorld.InfiniteScroll;
using Newtonsoft.Json.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace Sctipts.UI.DrawHolders
{
	[RequireComponent(typeof(Text))]
	public class TextHolder : ViewHolder, IDrawValue
	{
		public const string LinkType = "Text";

		[SerializeField]
		protected Text _text;

		public virtual void SetValue(JToken value)
		{
			_text.text = value["text"].ToString();
		}
	}
}
