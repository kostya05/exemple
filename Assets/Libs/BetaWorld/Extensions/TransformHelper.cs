﻿using UnityEngine;

namespace BetaWorld.Extensions
{
	public static class TransformHelper
	{
		public enum Direction
		{
			TopLeft,
			Top,
			TopRight,
			BottomLeft,
			Bottom,
			BottomRight,
			Left,
			Right,
			Center
		}

		private static readonly Vector3[] Corners = new Vector3[4];

		public static void ChangePosition(this RectTransform transform, Vector2 postion, Direction direction, Vector3[] corners = null)
		{
			var currentCorners = corners ?? Corners;
			if (corners == null)
				transform.GetWorldCorners(currentCorners);
			Vector2 pos = GetCorner(transform, direction, currentCorners);
			transform.position = transform.position - (Vector3)pos + (Vector3)postion;
		}

		public static Vector2 GetCorner(this RectTransform transform, Direction dir, Vector3[] corners = null)
		{
			var currentCorners = corners ?? Corners;
			if (corners == null)
				transform.GetWorldCorners(currentCorners);
			switch (dir)
			{
				case Direction.Left:
					return new Vector2(currentCorners[0].x, (currentCorners[0].y + currentCorners[1].y) / 2f);
				case Direction.Right:
					return new Vector2(currentCorners[3].x, (currentCorners[0].y + currentCorners[1].y) / 2f);
				case Direction.Top:
					return new Vector2((currentCorners[0].x + currentCorners[3].x) / 2f, currentCorners[1].y);
				case Direction.Bottom:
					return new Vector2((currentCorners[0].x + currentCorners[3].x) / 2f, currentCorners[0].y);
				case Direction.BottomLeft:
					return currentCorners[0];
				case Direction.BottomRight:
					return currentCorners[3];
			}
			return Vector2.zero;
		}

		public static Rect GetWorldRect(this RectTransform transform, Vector3[] corners = null)
		{
			var currentCorners = corners ?? Corners;
			if (corners == null)
				transform.GetWorldCorners(currentCorners);
			return new Rect(currentCorners[0].x, currentCorners[0].y, Mathf.Abs(currentCorners[2].x - currentCorners[0].x), Mathf.Abs(currentCorners[1].y - currentCorners[0].y));
		}
	}
}