﻿using System.Collections;
using BetaWorld.UnityBundleSystem;
using UnityEngine;
using UnityEngine.UI;

public class ExempleUnityBundleSystem : MonoBehaviour {

    private void Awake()
    {
        BundleSystem.Initialize();
    }

	// Use this for initialization
	IEnumerator Start ()
    {
        yield return new WaitUntil(() => BundleSystem.IsReady);
        var result = BundleSystem.GetObject(new CategoryObject("ExempleUnityBundleSystemColorCategory", "RR"));
        if(result != null)
	        GetComponent<Image>().color = (Color)result;
    }

}
