﻿using System.Collections.Generic;
using BetaWorld.Interfaces;
using UnityEngine;

namespace BetaWorld.Pool
{
	public class UnityObjectsPool : IPool
	{
		private readonly Transform _content;
		private readonly Dictionary<object, UnityObjectCategory> _categories = new Dictionary<object, UnityObjectCategory>();

		public UnityObjectsPool(Transform content)
		{
			_content = content;
		}

		public void Add(object type, Component obj)
		{
			if (_categories.ContainsKey(type))
			{
				_categories[type].Add(obj);
			}
			else
			{
				var category = new UnityObjectCategory(_content, type.ToString());
				category.Add(obj);
				_categories[type] = category;
			}
		}

		public void Add(object type, GameObject obj)
		{
			if (_categories.ContainsKey(type))
			{
				_categories[type].Add(obj);
			}
			else
			{
				var category = new UnityObjectCategory(_content, type.ToString());
				category.Add(obj);
				_categories[type] = category;
			}
		}

		public T Get<T>(object element) where T : Component
		{
			if (_categories.ContainsKey(element))
				return _categories[element].Take<T>();
			return null;
		}

		public GameObject Get(object element)
		{
			if (_categories.ContainsKey(element))
				return _categories[element].Take<GameObject>();
			return null;
		}
	}
}
