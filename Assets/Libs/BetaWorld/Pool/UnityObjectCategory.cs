﻿using System.Collections.Generic;
using UnityEngine;

namespace BetaWorld.Pool
{
	[System.Serializable]
	public class UnityObjectCategory
	{
		[SerializeField]
		private Transform _content;
		private readonly Queue<Object> _pool = new Queue<Object>();

		public UnityObjectCategory(Transform content, string name)
		{
			var go = new GameObject(name);
			_content = go.transform;
			_content.SetParent(content);	
		}
		public void Add(Component component)
		{
			_pool.Enqueue(component);
			component.transform.SetParent(_content, false);
		}

		internal void Add(GameObject obj)
		{
			_pool.Enqueue(obj);
			obj.transform.SetParent(_content);
		}

		public T Take<T>() where T : Object
		{
			while (_pool.Count > 0)
			{
				var element = _pool.Dequeue() as T;
				if (element != null)
					return element;
			}
			return null;
		}
	}
}
