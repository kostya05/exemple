﻿using BetaWorld.InfiniteScroll;
using UnityEngine;

namespace BetaWorld.Interfaces
{
	public interface IPool
	{
		void Add(object type, Component obj);
		void Add(object type, GameObject obj);
		T Get<T>(object element) where T : Component;
		GameObject Get(object element);
	}
}