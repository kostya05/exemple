﻿using System;
using System.Collections.Generic;
using BetaWorld.InfiniteScroll.Interfaces;
using JetBrains.Annotations;
using UnityEngine;
using IPool = BetaWorld.Interfaces.IPool;
using Object = UnityEngine.Object;

namespace BetaWorld.InfiniteScroll
{
   public abstract class ScrollAdapter<T,TM> : IScrollAdapter
   {
        protected readonly InfiniteScrollRect Owner;
        private Dictionary<Enum, Action> _bindenActions;
        private Dictionary<Enum, Action<object[]>> _bindenActionsObjects;

        private List<T> _tmpList;
        protected readonly List<T> DataSet;
        protected readonly ViewContent _content;

        private Dictionary<TM, ViewHolder> _typeInfoDictionary;
        protected abstract TM[] AllElementCategories { get; }
        protected Func<TM, GameObject> GetObject;
        protected ScrollAdapter([NotNull]InfiniteScrollRect owner,[NotNull]Func<TM, GameObject> getObject)
        {
            GetObject = getObject;
            if (DataSet == null)
                DataSet = new List<T>();

            Owner = owner;
            _content = owner.Content;
            CreateTypesMap();
        }

       public List<T> GetData()
       {
           return DataSet;
       }

        public void BindAdapterAction(Enum codeAction, Action action)
        {
            if(_bindenActions == null)
                _bindenActions = new Dictionary<Enum, Action>();

            _bindenActions[codeAction] = action;
        }

        public void BindAdapterAction(Enum codeAction, Action<object[]> action)
        {
            if (_bindenActionsObjects == null)
                _bindenActionsObjects = new Dictionary<Enum, Action<object[]>>();

            _bindenActionsObjects[codeAction] = action;
        }

        public void NotifyAction(Enum codeAction)
        {
            bool found = _bindenActions != null && _bindenActions.ContainsKey(codeAction);
            if(found)
                _bindenActions[codeAction].Invoke();
            else
                Debug.LogError("ScrollAdapter:: NotFound action by code: " + codeAction);
        }

        public void NotifyAction(Enum codeAction, object[] paramObjects)
        {
            bool found = _bindenActionsObjects != null && _bindenActionsObjects.ContainsKey(codeAction);
            if (found)
                _bindenActionsObjects[codeAction].Invoke(paramObjects);
            else
	            Debug.LogError("ScrollAdapter:: NotFound action with params by code: " + codeAction);
        }

        protected ScrollAdapter(InfiniteScrollRect owner, [NotNull]Func<TM, GameObject> getObvjetc, [NotNull]ICollection<T> data) : this(owner, getObvjetc)
        {
            DataSet = new List<T>(data.Count);
            DataSet.AddRange(data);
        }

        protected void CreateTypesMap()
        {
            _typeInfoDictionary = new Dictionary<TM, ViewHolder>(AllElementCategories.Length);
            foreach (var element in AllElementCategories)
            {
                 _typeInfoDictionary[element] = GetObject(element).GetComponent<ViewHolder>();
            }
        }

        public virtual int ItemCount
        {
            get { return DataSet.Count; }
        }

        public ViewHolder GetOrCreateViewHolder(int position)
        {
            var type = (TM)ElementType(position);
            ViewHolder vh = Owner.GetFreeHolder(type);
            if (vh == null)
            {
                var go = Object.Instantiate(GetObject(type), _content.RectTransform, false);
                vh = go.GetComponent<ViewHolder>();
            }
            else
                vh.transform.SetParent(_content.RectTransform, false);

            vh.BindHolderActions(GetNotifyAction, GetNotifyActionWithParams);
            vh.InitHolder(_content, type);
            return vh;
        }

        public abstract object ElementType(int position);
        public abstract Type GetHolderType(int position);

       public virtual void OnBindViewHolder(ViewHolder viewHolder, int position)
       {
           viewHolder.Position = position;
       }

       public ViewHolder GetViewHolderInfo(int postion)
       {
            return _typeInfoDictionary[(TM)ElementType(postion)];
       }

       public virtual object GetValue(int position)
       {
           return DataSet[position];
       }

       public virtual bool HaveHolder(string stringType, out object keyHolder)
       {
           foreach (var elementType in AllElementCategories)
           {
               var stringElementType = elementType.ToString();
               if (stringElementType.Equals(stringType))
               {
                   keyHolder = elementType;
                   return true;
               }
           }

           keyHolder = null;
           return false;
       }

       public virtual void TryInitPool(IPool poolElements)
       {
           
       }

       private Action<object[]> GetNotifyActionWithParams(Enum codeAction)
        {
            bool found = _bindenActionsObjects != null && _bindenActionsObjects.ContainsKey(codeAction);
            if (found)
                return _bindenActionsObjects[codeAction];
            return null;
        }

        private Action GetNotifyAction(Enum codeAction)
        {
            bool found = _bindenActions != null && _bindenActions.ContainsKey(codeAction);
            if (found)
                return _bindenActions[codeAction];
            return null;
        }

        public virtual void NotifyAll()
        {
            Notify(BaseScrollNotify.RedrawAndClear, typeof (BaseScrollNotify));
        }

        public virtual void Notify(Enum notifyCommand, Type type = null)
        {
            if (type == null)
                type = notifyCommand.GetType();

            if (type == typeof (BaseScrollNotify))
            {
                switch ((BaseScrollNotify) notifyCommand)
                {
                    case BaseScrollNotify.RedrawAndClear:
                        Owner.Reload(true);
                        break;
                    case BaseScrollNotify.Redraw:
                        Owner.Reload(false, false);
                        break;
                }
            }
        }

        public virtual void NotifyByElement(Enum notifyCommand, int position, Type typeEnum = null)
        {
            if (typeEnum == null)
                typeEnum = notifyCommand.GetType();
            
            if (typeEnum == typeof (BaseScrollNotify))
            {
                switch ((BaseScrollNotify)notifyCommand)
                {
                    case BaseScrollNotify.ChangeElement:
                        var viewHolder = Owner.TryGetViewHolderByPosition(position);
                        if(viewHolder != null)
                            OnBindViewHolder(viewHolder, position);
                        break;
                    case BaseScrollNotify.RemoveElement:
                    case BaseScrollNotify.AddElement:
                        Owner.Reload(false, false);
                        break;
                }
            }
        }

        public virtual void AddElement(T element)
        {
            DataSet.Add(element);
            NotifyByElement(BaseScrollNotify.AddElement, DataSet.Count - 1, typeof(BaseScrollNotify));
        }

        public virtual void AddElementByPosition(T element, int position)
        {
            DataSet.Insert(position, element);
            NotifyByElement(BaseScrollNotify.AddElement, position, typeof(BaseScrollNotify));
        }

        public virtual void RemoveElement(int position)
        {
            DataSet.RemoveAt(position);
            NotifyByElement(BaseScrollNotify.RemoveElement, position, typeof(BaseScrollNotify));
        }
       
       public void RemoveElementsWhere(Func<T, bool> where)
       {
            if (_tmpList == null)
                _tmpList = new List<T>();
            _tmpList.Clear();

            for (int i = 0; i < DataSet.Count; i++)
            {
                var element = DataSet[i];
                if (!where.Invoke(element))
                    _tmpList.Add(element);
            }
            DataSet.Clear();
            DataSet.AddRange(_tmpList);
            if(_tmpList.Count > 0)
                Notify(BaseScrollNotify.Redraw, typeof(BaseScrollNotify));
            _tmpList.Clear();
        }
    }
}
