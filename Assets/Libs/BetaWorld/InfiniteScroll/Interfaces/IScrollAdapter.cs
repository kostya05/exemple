﻿using BetaWorld.Interfaces;

namespace BetaWorld.InfiniteScroll.Interfaces
{
    public enum BaseScrollNotify
    {
        AddElement,
        RemoveElement,
        ChangeElement,
        Redraw,
        RedrawAndClear
    }

    public interface IScrollAdapter : INotify, INotifyCollection
    {
        int ItemCount { get; }
        System.Object ElementType(int position);
        ViewHolder GetOrCreateViewHolder(int position);
        void OnBindViewHolder(ViewHolder viewHolder, int position);
        ViewHolder GetViewHolderInfo(int postion);

        object GetValue(int position);
        bool HaveHolder(string stringType, out object keyHolder);
        void TryInitPool(IPool poolElements);
    }
}