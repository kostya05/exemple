﻿using System;

namespace BetaWorld.InfiniteScroll.Interfaces
{
	public interface INotifyCollection
	{
		void NotifyByElement(Enum notifyCommand, int position, Type typeEnum = null);
	}
}