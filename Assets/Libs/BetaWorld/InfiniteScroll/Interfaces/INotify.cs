﻿using System;

namespace BetaWorld.InfiniteScroll.Interfaces
{
	public interface INotify
	{
		void NotifyAll();

		void Notify(Enum notifyCommand, Type type = null);
	}
}