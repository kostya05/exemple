﻿using System;
using System.Linq;
using BetaWorld.Extensions;
using UnityEngine;
using Direction = BetaWorld.Extensions.TransformHelper.Direction;


namespace BetaWorld.InfiniteScroll
{
    public class VisibleObject : IDisposable
    {
	    private static readonly Vector3[] _tmpCorrner = new Vector3[4];

		private readonly ViewContent _viewContent;
        private int _currantFrame = -1;
        private RectTransform _tr;
        private readonly Vector3[] _currentPositionsCorners = new Vector3[4];
		
		public VisibleObject(ViewContent content, RectTransform tr)
        {
            _viewContent = content;
            //_viewContent.Update += ViewContentOnUpdate;
            _tr = tr;
        }

        public bool GetVisible(Direction dir)
        {
            Calc();
            return GetCachigVisible(dir);
        }

        public bool GetCachigVisible(Direction dir)
        {
            if (Time.frameCount != _currantFrame)
                Calc();

            var viewportCorners = _viewContent.CurrentCorners;
            switch (dir)
            {
                case Direction.Bottom:
                    return viewportCorners[0].y < _currentPositionsCorners[1].y;
                case Direction.Top:
                    return viewportCorners[1].y > _currentPositionsCorners[0].y;
                case Direction.Left:
                    return viewportCorners[0].x < _currentPositionsCorners[3].x;
                case Direction.Right:
                    return viewportCorners[3].x > _currentPositionsCorners[0].x;
            }
            return false;
        }

        public static void NormalizeCorner(Vector3[] corrner)
        {
            lock (_tmpCorrner)
            {
                for (int i = 0; i < 4; i++)
                {
                    _tmpCorrner[i] = corrner[i];
                }
                for (int i = 0; i < 4; i++)
                {
                    if (corrner[i].x <= _tmpCorrner[0].x && corrner[i].y <= _tmpCorrner[0].y)
                        _tmpCorrner[0] = corrner[i];
                    if (corrner[i].x <= _tmpCorrner[1].x && corrner[i].y >= _tmpCorrner[1].y)
                        _tmpCorrner[1] = corrner[i];
                    if (corrner[i].x >= _tmpCorrner[2].x && corrner[i].y >= _tmpCorrner[2].y)
                        _tmpCorrner[2] = corrner[i];
                    if (corrner[i].x >= _tmpCorrner[3].x && corrner[i].y <= _tmpCorrner[3].y)
                        _tmpCorrner[3] = corrner[i];
                }
                for (int i = 0; i < 4; i++)
                {
                    corrner[i] = _tmpCorrner[i];
                }
            }
        }

        public Vector2 GetCorner(Direction dir)
        {
            if (Time.frameCount != _currantFrame)
                Calc();

            return TransformHelper.GetCorner(null, dir, _currentPositionsCorners);
        }

        private void Calc()
        {
            _currantFrame = Time.frameCount;
            _tr.GetWorldCorners(_currentPositionsCorners);
            NormalizeCorner(_currentPositionsCorners);
        }

        private void ViewContentOnUpdate()
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            //_viewContent.Update -= ViewContentOnUpdate;
            _tr = null;
        }
    }
}
