﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using BetaWorld.Extensions;
using BetaWorld.InfiniteScroll.Interfaces;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using IPool = BetaWorld.Interfaces.IPool;

namespace BetaWorld.InfiniteScroll
{
    public class InfiniteScrollRect : UIBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler, IInitializePotentialDragHandler//, IScrollHandler
    {
        public enum DirectionVariants : byte
        {
            Horizontal,
            Vertical
        }

        public enum MoveProcessingVariants : byte
        {
            Normal,
            Centring,
            Inertia
        }

        public enum BoundariesProcessingVariants : byte
        {
            Normal,
            Elastic
        }

        public enum States
        {
            None,
            Drag,
            Elastic,
            Centring
        }

        public enum DirOffset : byte
        {
            None, Left, Right
        }

#region Const

        private const int ItemDifference = 3;
        private const int InfelicityOffset = 6;
        #endregion

        #region InspectorValues
        [SerializeField]
        private DirectionVariants _direction = DirectionVariants.Horizontal;
        [SerializeField]
        private MoveProcessingVariants _moveType = MoveProcessingVariants.Normal;
        [SerializeField]
        private BoundariesProcessingVariants _boundariesProcessing = BoundariesProcessingVariants.Normal;

        [SerializeField]
        private HorizontalOrVerticalLayoutGroup _layoutGroup;

        [SerializeField]
        private RectTransform _content;
		
        [SerializeField]
        private bool _allwaysCanMove;

        [SerializeField]
        private bool _canMove = true;

        [SerializeField]
        private float _decelerationRate = 0.135f;

        [SerializeField]
        private float _elasticityForce = 0.1f;

        [SerializeField, Range(0.1f, 1f)]
        private float _elasticityDragFroce = 1f;

        [SerializeField]
        private float _elasticSize = 10f;

        [SerializeField] private float _centringFactor = 1000f;

        [SerializeField]
        private float _centringForce = 0.1f;
        #endregion

        public DirectionVariants DirectionScroll { get { return _direction; } }
        public MoveProcessingVariants MoveType { get { return _moveType; } }
        public BoundariesProcessingVariants BoundariesProcessing { get { return _boundariesProcessing; } }

        public DirOffset CurrnetOffset { get; protected set; }

        public int MoveAxis { get { return DirectionScroll == DirectionVariants.Horizontal ? 0 : 1; } }

        public States CurrentState { get; private set; }

        public HorizontalOrVerticalLayoutGroup LayoutGroup { get { return _layoutGroup; } }

        public bool CanMove { get { return _canMove; } set { _canMove = value; } }

        public int LeftPosition { get { return _positionLeft; } }
        public int RightPosition { get { return _positionRight; } }

        public float CentringForce { get { return _centringForce; } }

	    public bool IsInitialize
		{
		    get { return Adapter != null && _poolElements != null; }
	    }

	    protected bool AllwaysCanMove
        {
            get { return _allwaysCanMove; }
            set { _allwaysCanMove = value; }
        }

        public ViewContent Content { get; private set; }

        protected IScrollAdapter Adapter;
        protected Vector2 PositionMinMax;
        protected Rect ContentSize;
        protected RectTransform ViewRectTransform;
        protected Vector2 Velocity;
        protected bool ElasticWork;
        protected float PrevPosition;
        protected Vector2 CacheOffset;

        protected float[] PositionsElementsCenter;

        private IPool _poolElements;
        private KeyValuePair<object, ViewHolder>? _freeElement;

        private LinkedList<ViewHolder> _left;
        private LinkedList<ViewHolder> _itemsHandlers;
        private LinkedList<ViewHolder> _right;      

        private int _positionLeft;
        private int _positionRight;

        private Vector2 _pointerStartLocalCursor;
        private Vector2 _contentStartPosition;        
        
        [Serializable]
        public class InfiniteScrollChangeValue : UnityEvent<float> { }

        public InfiniteScrollChangeValue OnVisibleContentSizeChange;
        public InfiniteScrollChangeValue OnNormolizePositionChange;

        public bool IsExecuteEventHandlerHierarchyUp = true;
        private GameObject _parantGo;
        private PointerEventData _lastEventData;
		
        private readonly Dictionary<object, Stack<ViewHolder>> _redrawPool = new Dictionary<object, Stack<ViewHolder>>(); 
        
        public float Normalize
        {
            get
            {
                float length = PositionMinMax.y - PositionMinMax.x;
                if (length < 0f || Mathf.Approximately(length, 0f))
                    return 0f;
                return (_content.position[MoveAxis] - PositionMinMax.x)/length;
            }
            set
            {
                Vector3 result = _content.position;
                result[MoveAxis] = Mathf.Lerp(PositionMinMax.x, PositionMinMax.y, value);
                _content.position = result;
            }
        }

        public IEnumerable GetAllHolders()
        {
            return _left.Concat(_itemsHandlers).Concat(_right);
        }

        private TransformHelper.Direction GetVisibleDir(bool right)
        {
            switch (DirectionScroll)
            {
                case DirectionVariants.Horizontal:
                    return right ? TransformHelper.Direction.Left : TransformHelper.Direction.Right;
                case DirectionVariants.Vertical:
                    return right ? TransformHelper.Direction.Top : TransformHelper.Direction.Bottom;
            }
            return TransformHelper.Direction.Center;
        }

        public void SetAdapter(IScrollAdapter adapter)
        {
            Adapter = adapter;
            if (_poolElements != null)
                TryInitPoolWithAdapter(adapter);
            Reload(true);
        }

        private void TryInitPoolWithAdapter(IScrollAdapter adapter)
        {
            adapter.TryInitPool(_poolElements);
        }

        [ContextMenu("Reload")]
        public void Reload()
        {
            Reload(false);
        }
		
        private void TryMoveToFirstPool(LinkedList<ViewHolder> collections)
        {
            var node = collections.First;
            while (node != null)
            {
                var vh = node.Value;
				Stack<ViewHolder> stack;
	            if (_redrawPool.ContainsKey(vh.CurrentType))
		            stack = _redrawPool[vh.CurrentType];
	            else
		            stack = _redrawPool[vh.CurrentType] = new Stack<ViewHolder>();

	            stack.Push(vh);
				node = node.Next;
            }
        }
        
        public void Reload(bool clear, bool normalize = true, float normalizeValue = 0f)
        {
            if(!IsInitialize)
                return;

            var contentPosition = _content.position;

            if (clear)
                ClearAndDestroy();
            else
            {
                TryMoveToFirstPool(_itemsHandlers);
                TryMoveToFirstPool(_right);
                TryMoveToFirstPool(_left);

                _freeElement = null;
                _left.Clear();
                _right.Clear();
                _itemsHandlers.Clear();
            }
            
            CalculateContent();

            if (Adapter.ItemCount > 0)
                BeginDraw();
            
            foreach (var stack in _redrawPool.Values)
            {
                while (stack.Count > 0)
                {
                    var vh = stack.Pop();
                    _poolElements.Add(vh.CurrentType, vh);
                }
            }
            //if (!clear)
            //    GetCurrentPossition();

            if (normalize)
            {
                Normalize = normalizeValue;
            }
            else
            {
                //var offset = CalculateOffset(contentPosition, false)[MoveAxis];

                if (Adapter.ItemCount == 0 || (!AllwaysCanMove && (Adapter.ItemCount <= 1 || !CanMove)))
                    Normalize = 0f;
                else
                    _content.position = contentPosition;
            }

            var axis = MoveAxis;
            float visibleSize = ViewRectTransform.lossyScale[axis] * ViewRectTransform.rect.size[axis];
            OnVisibleContentSizeChange.Invoke(Mathf.Min(Mathf.Abs(visibleSize / ContentSize.size[axis]), 1f));
        }

        private void ClearAndDestroy()
        {
            foreach (var holder in _left)
                _poolElements.Add(holder.CurrentType, holder);
            foreach (var holder in _right)
                _poolElements.Add(holder.CurrentType, holder);
            foreach (var holder in _itemsHandlers)
                _poolElements.Add(holder.CurrentType, holder);

            _left.Clear();
            _right.Clear();
            _itemsHandlers.Clear();

            var rectTr = Content.RectTransform;
            for (int i = 0; i < rectTr.childCount; i++)
            {
                var element = rectTr.GetChild(i);
                if(element != null)
                    Destroy(element.gameObject);
            }

            _freeElement = null;
            //_poolElements.Clear();
        }

        protected override void Awake()
        {
            ViewRectTransform = (RectTransform) transform;
            if (transform == null)
                IsExecuteEventHandlerHierarchyUp = false;
            else if(transform.parent != null)
                _parantGo = transform.parent.gameObject;
            Content = new ViewContent(_content);
            _left = new LinkedList<ViewHolder>();
            _itemsHandlers = new LinkedList<ViewHolder>();
            _right = new LinkedList<ViewHolder>();

            if (Adapter != null)
                TryInitPoolWithAdapter(Adapter);
        }

	    public void SetPool(IPool pool)
	    {
		    _poolElements = pool;
	    }

        protected override void Start()
        {
            base.Start();
            if (_parantGo == null && transform != null)
                _parantGo = transform.parent.gameObject;
        }

        protected void ResetStates()
        {
            ElasticWork = false;
        }

        protected virtual void CalculateCentrerPositions(out float[] positionsCenter)
        {
            int countItem = Adapter.ItemCount;
            var viewHolderInfo = Adapter.GetViewHolderInfo(0);
            var moveAxis = MoveAxis;
            var lossyScale = transform.lossyScale[moveAxis];
            positionsCenter = new float[countItem - 1];

            var padding = DirectionScroll == DirectionVariants.Horizontal
                ? LayoutGroup.padding.left
                : LayoutGroup.padding.top;
            
            float position = PositionMinMax.x - padding * lossyScale + (viewHolderInfo.IsVariableSize ? viewHolderInfo.GetSize(Adapter.GetValue(0), ViewRectTransform, moveAxis) : viewHolderInfo.rectTransform.rect.size[moveAxis]) * lossyScale + LayoutGroup.spacing * lossyScale;
            
            for (int i = 0; i < countItem - 1; i++)
            {
                position -= lossyScale * (viewHolderInfo.IsVariableSize ? viewHolderInfo.GetSize(Adapter.GetValue(i + 1), ViewRectTransform, moveAxis) : viewHolderInfo.rectTransform.rect.size[moveAxis]);
                positionsCenter[i] = position;
                position -= LayoutGroup.spacing*lossyScale;
            }
        }

        public void CalculateContent()
        {
            CurrnetOffset = DirOffset.None;
            int moveAxis = MoveAxis;
            float lossyScale = transform.lossyScale[moveAxis];

            Vector2 minPos = Vector2.zero;
            switch (DirectionScroll)
            {
                case DirectionVariants.Horizontal:
                    minPos = ViewRectTransform.GetCorner(TransformHelper.Direction.Left) + LayoutGroup.padding.left * lossyScale * Vector2.right;
                    break;
                case DirectionVariants.Vertical:
                    minPos = ViewRectTransform.GetCorner(TransformHelper.Direction.Top) - LayoutGroup.padding.top * lossyScale * Vector2.up;
                    break;
            }

            ContentSize = new Rect(minPos, new Vector2(0f, 0f));
            var vectorSize = ContentSize.size;
            var dir = DirectionScroll;
            switch (dir)
            {
                case DirectionVariants.Horizontal:
                    vectorSize.y = ViewRectTransform.rect.height * lossyScale;
                    vectorSize.x = LayoutGroup.padding.horizontal * lossyScale;
                    break;
                case DirectionVariants.Vertical:
                    vectorSize.x = ViewRectTransform.rect.width * lossyScale;
                    vectorSize.y = LayoutGroup.padding.vertical * lossyScale;
                    break;
                default:
                    return;
            }
            
            for (int i = 0; i < Adapter.ItemCount; i++)
            {
                var viewHolderInfo = Adapter.GetViewHolderInfo(i);
                var element = Adapter.GetValue(i);
                float size = lossyScale * (viewHolderInfo.IsVariableSize
                    ? viewHolderInfo.GetSize(element, ViewRectTransform, moveAxis)
                    : viewHolderInfo.rectTransform.rect.size[moveAxis]);
                vectorSize[moveAxis] += size;
            }
            

            if (AllwaysCanMove)
            {
                //vectorSize[moveAxis] += LayoutGroup.spacing * lossyScale * (Adapter.ItemCount - 1);
                var viewRectSize = ViewRectTransform.rect.size[moveAxis] * ViewRectTransform.lossyScale[moveAxis];
                if (vectorSize[moveAxis] <= viewRectSize)
                    vectorSize[moveAxis] = viewRectSize + 1f;
            }
            
            ContentSize.size = vectorSize;
            switch (dir)
            {
                    case DirectionVariants.Horizontal:
                    var leftH = minPos[moveAxis];// + _viewRect.rect.size[moveAxis] / 2 * lossyScale;
                    PositionMinMax = new Vector2(leftH, leftH - ContentSize.width - LayoutGroup.padding.right * lossyScale + ViewRectTransform.rect.size[moveAxis] / 2f * lossyScale);
                    break;
                    case DirectionVariants.Vertical:
                    var leftV = minPos[moveAxis];
                    float max = leftV + ContentSize.height + LayoutGroup.padding.bottom*lossyScale -
                                ViewRectTransform.rect.size[moveAxis]*lossyScale;
                    PositionMinMax = new Vector2(leftV, Mathf.Max(max, leftV + LayoutGroup.padding.bottom * lossyScale));
                    
                    break;
            }
        }

#if UNITY_EDITOR
        private void OnDrawGizmos()
        {
            if(!Application.isPlaying)
                return;
            Gizmos.color = Color.red;
            Vector3 begin = ContentSize.position;
            begin.z = Content.RectTransform.position.z;
            Vector3 end = begin;
            float lossyScale = transform.lossyScale[MoveAxis];
            switch (DirectionScroll)
            {
                case DirectionVariants.Horizontal:
                    end.x -= ContentSize.width - LayoutGroup.padding.right * lossyScale;
                    break;
                case DirectionVariants.Vertical:
                    end.y += ContentSize.height - LayoutGroup.padding.horizontal * lossyScale;
                    break;
            }

            Gizmos.DrawLine(begin, end);
        }
#endif

        private void BeginDraw()
        {
            _positionLeft = 0;
            _positionRight = -1;

            //_layoutGroup.CalculateLayoutInputHorizontal();
            Content.RectTransform.ChangePosition(ContentSize.position, DirectionScroll == DirectionVariants.Horizontal ? TransformHelper.Direction.Left : TransformHelper.Direction.Top);

            /*switch (DirectionScroll)
            {
                case DirectionVariants.Horizontal:
                    //_contentSize.position += new Vector2(LayoutGroup.spacing * transform.lossyScale[MoveAxis], 0);
                    break;
                case DirectionVariants.Vertical:
                    //_contentSize.position -= new Vector2(0, LayoutGroup.spacing * transform.lossyScale[MoveAxis]);
                    break;
            }*/
            
            var item = DrawFist();
            _itemsHandlers.AddLast(item);
            
            const int itemDifferenceDouble = ItemDifference * 2;
            _positionRight++;
            while (_right.Count != itemDifferenceDouble && _positionRight < Adapter.ItemCount)
            {
                var vh = DrawNext(true);

                var visable = vh.VisibleObject.GetVisible(DirectionScroll == DirectionVariants.Horizontal ? TransformHelper.Direction.Right : TransformHelper.Direction.Bottom);
                var collection = visable ? _itemsHandlers : _right;
                collection.AddLast(vh);

                _positionRight++;
            }
            _positionRight--;

            CalculateCentrerPositions(out PositionsElementsCenter);

            var moveAxis = MoveAxis;
            CanMove = ContentSize.size[moveAxis] >= ViewRectTransform.rect.size[moveAxis] * ViewRectTransform.lossyScale[moveAxis];
        }

        private static Vector2 GetPositionBy(ViewHolder vh, bool right, DirectionVariants dir)
        {
            if(right)
                return vh.rectTransform.GetCorner(dir == DirectionVariants.Horizontal ? TransformHelper.Direction.Right : TransformHelper.Direction.Bottom);
            else
                return vh.rectTransform.GetCorner(dir == DirectionVariants.Horizontal ? TransformHelper.Direction.Left : TransformHelper.Direction.Top);
        }

        private ViewHolder DrawFist()
        {
            _positionRight++;
            Vector2 position = ContentSize.position;
            ViewHolder vh = null;
			var elementType = Adapter.ElementType(_positionRight);
	        if (_redrawPool.ContainsKey(elementType))
	        {
		        var stack = _redrawPool[elementType];
		        if (stack.Count > 0)
			        vh = stack.Pop();
	        }
	        if (vh == null)
		        vh = Adapter.GetOrCreateViewHolder(_positionRight);
	        Adapter.OnBindViewHolder(vh, _positionRight);
			vh.name = _positionRight.ToString();
	        vh.rectTransform.ChangePosition(position, DirectionScroll == DirectionVariants.Horizontal ? TransformHelper.Direction.Left : TransformHelper.Direction.Top);

			return vh;
        }

        private ViewHolder DrawNext(bool right)
        {
            var prevElement = right ? GetRightHandler() : GetLeftHandler();
            Vector2 prevPos = GetPositionBy(prevElement, right, DirectionScroll);
            
            var positionNext = right ? _positionRight : _positionLeft;

            ViewHolder vh = null;
			var elementType = Adapter.ElementType(positionNext);
	        if (_redrawPool.ContainsKey(elementType))
	        {
		        var stack = _redrawPool[elementType];
		        if (stack.Count > 0)
			        vh = stack.Pop();
	        }
	        if (vh == null)
		        vh = Adapter.GetOrCreateViewHolder(positionNext);
			ContentPositionAddSpace(ref prevPos, right);

			TransformHelper.Direction dir;
	        if (right)
		        dir = DirectionScroll == DirectionVariants.Horizontal ? TransformHelper.Direction.Left : TransformHelper.Direction.Top;
	        else
		        dir = DirectionScroll == DirectionVariants.Horizontal ? TransformHelper.Direction.Right : TransformHelper.Direction.Bottom;

	        Adapter.OnBindViewHolder(vh, positionNext);
	        vh.name = positionNext.ToString();
	        vh.rectTransform.ChangePosition(prevPos, dir);

			return vh;
        }

        private void ContentPositionAddSpace(ref Vector2 position, bool right)
        {
            switch (DirectionScroll)
            {
                case DirectionVariants.Horizontal:
                    if(right)
                        position += new Vector2(LayoutGroup.spacing, 0f) * transform.lossyScale.x;
                    else
                        position -= new Vector2(LayoutGroup.spacing, 0f) * transform.lossyScale.x;
                    break;
                case DirectionVariants.Vertical:
                    if(right)
                        position -= new Vector2(0, LayoutGroup.spacing) * transform.lossyScale.y;
                    else
                        position += new Vector2(0, LayoutGroup.spacing) * transform.lossyScale.y;
                    break;
            }
        }

        protected ViewHolder GetLeftHandler()
        {
            if (_left.Count > 0)
            {
                if (_left.Count == 1)
                    return _left.First.Value;
                return _left.Last.Value;
            }
                
            if (_itemsHandlers.Count != 0)
                return _itemsHandlers.First.Value;
            return null;
        }

        protected ViewHolder GetRightHandler()
        {
            if (_right.Count > 0)
            {
                return _right.Last.Value;
            }
            if(_itemsHandlers.Count != 0)
                return _itemsHandlers.Last.Value;
            return null;
        }

        protected virtual void Update()
        {
            UpdateItems();
        }

        protected virtual void LateUpdate()
        {
            if(!IsInitialize || !CanMove)
                return;

            switch (BoundariesProcessing)
            {
                case BoundariesProcessingVariants.Elastic:
                    ElasticDetect();
                    break;
            }

            if (CurrentState == States.None || CurrentState == States.Elastic)
            {
                ApplyVelocity();
            }

            if (CurrentState != States.Elastic)
            {
                switch (MoveType)
                {
                    case MoveProcessingVariants.Inertia:
                        InertiaPostCalculate();
                        break;
                    case MoveProcessingVariants.Centring:
                        CentringPostCalculate();
                        break;
                }
            }
            

            if (!Mathf.Approximately(Content.RectTransform.position[MoveAxis], PrevPosition))
                UpdatePrevFrameData();
        }

        protected virtual void CentringPostCalculate()
        {
            if (CurrentState != States.Drag)
                return;

            float deltaTime = Time.unscaledDeltaTime;
            var moveAxis = MoveAxis;

            Vector3 newVelocity = Velocity;
            newVelocity[moveAxis] = (Content.RectTransform.position[moveAxis] - PrevPosition) / deltaTime;
            Velocity = Vector3.Lerp(Velocity, newVelocity, deltaTime * 10f);
        }

        private bool TryUpdateItemsTo(bool right)
        {
            int i = 0;
            if (right)
            {
                var mainLeft = _itemsHandlers.First.Value;
                while (TryChangePositions(mainLeft, true))
                {
                    mainLeft = _itemsHandlers.First.Value;
                    i++;
                }
            }
            else
            {
                var mainRight = _itemsHandlers.Last.Value;
                while (TryChangePositions(mainRight, false))
                {
                    mainRight = _itemsHandlers.Last.Value;
                    i++;
                }
            }
            return i > 0;
        }

        private void UpdateItems()
        {
            if (_itemsHandlers == null || _itemsHandlers.Count <= 1)
                return;
            if (!TryUpdateItemsTo(true))
                TryUpdateItemsTo(false);
        }

        private bool TryChangePositions(ViewHolder vh, bool right)
        {
            if (right && _positionRight == Adapter.ItemCount - 1 || !right && _positionLeft == 0)
                return false;
            var fistCat = right ? _left : _right;
            var endCat = right ? _right : _left;
            if (!vh.VisibleObject.GetVisible(GetVisibleDir(right)))
            {
                fistCat.AddFirst(vh);
                if (fistCat.Count >= ItemDifference)
                {
                    var leftOverScreen = fistCat.Last.Value;
                    fistCat.Remove(leftOverScreen);
                    _freeElement = new KeyValuePair<object, ViewHolder>(leftOverScreen.CurrentType, leftOverScreen);
                    if (right)
                    {
                        _positionRight++;
                        _positionLeft++;
                    }
                    else
                    {
                        _positionRight--;
                        _positionLeft--;
                    }
                    var next = DrawNext(right);
                    endCat.AddLast(next);
                }

                if (right)
                    _itemsHandlers.RemoveFirst();
                else
                    _itemsHandlers.RemoveLast();
                if (endCat.Count != 0)
                {
                    var rightOverScreen = endCat.First;
                    endCat.RemoveFirst();
                    if(right)
                        _itemsHandlers.AddLast(rightOverScreen);
                    else
                        _itemsHandlers.AddFirst(rightOverScreen);
                }
                return true;
            }
            return false;
        }

        #region MoveProcessing

        protected virtual void ElasticCalculate(ref Vector2 position, float deltaTime)
        {
            int moveAxis = MoveAxis;
            if (Mathf.Abs(CacheOffset[moveAxis]) < InfelicityOffset * ViewRectTransform.lossyScale[moveAxis])
            {
                Velocity[moveAxis] = 0f;
            }
            else
            {
                float speed = Velocity[moveAxis];
                var force = _elasticityForce;
                float offset = Mathf.Abs(CacheOffset[moveAxis]);
                if (offset < 1f)
                    force /= 10f;
                position[moveAxis] = Mathf.SmoothDamp(position[moveAxis], position[moveAxis] + CacheOffset[moveAxis], ref speed, force, Mathf.Infinity, deltaTime);
                Velocity[moveAxis] = speed;
                Velocity[moveAxis] = offset < 1f ? 0f : speed;
            }
        }

        private Vector2 CalculateOffset(Vector2 position, bool setOffset)
        {
            int moveAxis = MoveAxis;
            float currentPosition = (position != Vector2.zero ? position : (Vector2)Content.RectTransform.position)[moveAxis];

            switch (DirectionScroll) //Разница в знаках сравнения
            {
                case DirectionVariants.Horizontal:
                    if (currentPosition < PositionMinMax.y - InfelicityOffset * ViewRectTransform.lossyScale[moveAxis])
                    {
                        currentPosition = PositionMinMax.y - currentPosition;
                        if (setOffset)
                            CurrnetOffset = DirOffset.Right;
                    }
                    else if (currentPosition > PositionMinMax.x + InfelicityOffset * ViewRectTransform.lossyScale[moveAxis])
                    {
                        currentPosition = PositionMinMax.x - currentPosition;
                        if (setOffset)
                            CurrnetOffset = DirOffset.Left;
                    }
                    else
                    {
                        if (setOffset)
                            CurrnetOffset = DirOffset.None;
                        currentPosition = 0;
                    }
                    break;
                case DirectionVariants.Vertical:
                    if (currentPosition > PositionMinMax.y + InfelicityOffset * ViewRectTransform.lossyScale[moveAxis])
                    {
                        currentPosition = PositionMinMax.y - currentPosition;
                        if (setOffset)
                            CurrnetOffset = DirOffset.Left;
                    }
                    else if (currentPosition < PositionMinMax.x - InfelicityOffset * ViewRectTransform.lossyScale[moveAxis])
                    {
                        currentPosition = PositionMinMax.x - currentPosition;
                        if (setOffset)
                            CurrnetOffset = DirOffset.Right;
                    }
                    else
                    {
                        if (setOffset)
                            CurrnetOffset = DirOffset.None;
                        currentPosition = 0;
                    }
                    break;
            }
            
            var offset = Vector2.zero;
            offset[moveAxis] = currentPosition;
            return offset;
        }

        protected virtual void NormalMove(PointerEventData eventData)
        {
            if (CurrentState != States.Drag)
                return;

            var moveAxis = MoveAxis;
            Vector2 localCursor;

            if (!RectTransformUtility.ScreenPointToLocalPointInRectangle(ViewRectTransform, eventData.position, eventData.pressEventCamera, out localCursor))
                return;

            var pointerDelta = localCursor - _pointerStartLocalCursor;
            Vector2 position = _contentStartPosition + pointerDelta * ViewRectTransform.lossyScale[moveAxis];
            switch (BoundariesProcessing)
            {
                case BoundariesProcessingVariants.Normal:
                    Claped(ref position);
                    break;
                case BoundariesProcessingVariants.Elastic:
                    var offset = CalculateOffset(position, false);
                    position[moveAxis] = position[moveAxis] - RubberDelta(offset[moveAxis], _elasticSize);//ViewRectTransform.rect.size[moveAxis] * ViewRectTransform.lossyScale[moveAxis]);
                    break;
            }
            SetContentPosition(position);
        }

        private float RubberDelta(float overStretching, float viewSize)
        {
            var value = (1f - (1f/((Mathf.Abs(overStretching) * _elasticityDragFroce / viewSize) + 1f))) * viewSize;
            if (overStretching > 0f)
                value *= -1;
            return value;
        }

        protected virtual void ApplyVelocity()
        {
            var moveAxis = MoveAxis;
            //if (Mathf.Approximately(_velocity[moveAxis], 0f))
            //    return;
            float deltaTime = Time.unscaledDeltaTime;
            Vector2 position = Content.RectTransform.position;

            if (CurrentState == States.Elastic)
            {
                ElasticCalculate(ref position, deltaTime);
            }
            else if (MoveType == MoveProcessingVariants.Inertia)
            {
                InertiaCalculate(ref position, deltaTime);
            }
            else if (MoveType == MoveProcessingVariants.Centring)
            {
                CentringCalculate(ref position, deltaTime);
            }
            else
                Velocity[moveAxis] = 0f;

            if (!Mathf.Approximately(Velocity[moveAxis], 0f))
            {
                if(BoundariesProcessing == BoundariesProcessingVariants.Normal)
                    Claped(ref position);
                SetContentPosition(position);
            }
        }

        protected virtual void ElasticDetect()
        {
            CacheOffset = CalculateOffset(Vector2.zero, CurrentState == States.None || CurrentState == States.Elastic);
            if(CurrnetOffset != DirOffset.None)
                CurrentState = States.Elastic;
            else if(CurrentState == States.Elastic)
                CurrentState = States.None;
        }

        protected virtual void InertiaCalculate(ref Vector2 position, float deltaTime)
        {
            var moveAxis = MoveAxis;
            Velocity[moveAxis] *= Mathf.Pow(_decelerationRate, deltaTime);
            if (Mathf.Abs(Velocity[moveAxis]) < 1f)
                Velocity[moveAxis] = 0f;
            position[moveAxis] += Velocity[moveAxis] * deltaTime;
        }

        protected virtual void CentringCalculate(ref Vector2 position, float deltaTime)
        {
            var moveAxis = MoveAxis;
            Velocity[moveAxis] *= Mathf.Pow(_decelerationRate, deltaTime);
            if (Mathf.Abs(Velocity[moveAxis]) < 1f)
                Velocity[moveAxis] = 0f;

            var absVelocity = Mathf.Abs(Velocity[moveAxis])/transform.lossyScale[moveAxis];
            if (absVelocity < _centringFactor && absVelocity > 0.1f)
            {
                BeginCalculateCentring(Velocity[moveAxis] > 0f);
            }
            else
            {
                position[moveAxis] += Velocity[moveAxis] * deltaTime;
            }
        }

        protected virtual void BeginCalculateCentring(bool velocityDir)
        {
            var moveAxis = MoveAxis;
            bool rightDir = false;
            bool noneDir = true;

            float startSpeed = Velocity[moveAxis];
            Velocity[moveAxis] = 0f;
            int targetIndex = 0;
            var minDist = float.MaxValue;
            int i = 0;
            foreach (var pos in PositionsElementsCenter)
            {
                var dist = Mathf.Abs(_content.position[moveAxis] - pos);
                if (dist < minDist)
                {
                    rightDir = _content.position[moveAxis] - pos < 0;
                    noneDir = rightDir != velocityDir;
                    targetIndex = i;
                    minDist = dist;
                }
                i++;
            }

            if (noneDir && minDist / transform.lossyScale[moveAxis] > 60)
            {
                if (rightDir && targetIndex < Adapter.ItemCount - 1)
                    targetIndex++;
                else if (!rightDir && targetIndex > 1)
                    targetIndex--;
            }

            StartCoroutine(MoveCentring(PositionsElementsCenter[targetIndex], startSpeed));
        }

        private IEnumerator MoveCentring(float target, float startSpeed)
        {
            float time = 0f;
            float speed = startSpeed;
            bool isComplite = false;
            var moveAxis = MoveAxis;
            while (!isComplite && CurrentState != States.Drag)
            {
                yield return null;
                /*time += Time.unscaledDeltaTime;
                if (time >= 1)
                {
                    time = 1;
                    isComplite = true;
                }*/
                var positionCurrent = _content.position;
                positionCurrent[moveAxis] = Mathf.SmoothDamp(_content.position[moveAxis], target, ref speed, _centringForce, Mathf.Infinity, Time.unscaledDeltaTime);
                if (Mathf.Abs(_content.position[moveAxis] - target) < 2f* transform.lossyScale[MoveAxis])
                    isComplite = true;

                _content.position = positionCurrent;
            }
        }

        protected virtual void InertiaPostCalculate()
        {
            if(CurrentState != States.Drag)
                return;

            float deltaTime = Time.unscaledDeltaTime;
            var moveAxis = MoveAxis;

            Vector3 newVelocity = Velocity;
            newVelocity[moveAxis] = (Content.RectTransform.position[moveAxis] - PrevPosition) / deltaTime;
            Velocity = Vector3.Lerp(Velocity, newVelocity, deltaTime * 10);
        }

        protected virtual void UpdatePrevFrameData()
        {
            PrevPosition = Content.RectTransform.position[MoveAxis];
            OnNormolizePositionChange.Invoke(Normalize);
        }

        #endregion

        private void Claped(ref Vector2 position)
        {
            int moveAxis = MoveAxis;

            switch (DirectionScroll)
            {
                case DirectionVariants.Horizontal:
                    if (position[moveAxis] <= PositionMinMax.y)
                    {
                        position[moveAxis] = PositionMinMax.y;
                        Velocity = Vector2.zero;
                    }
                    else if (position[moveAxis] >= PositionMinMax.x)
                    {
                        position[moveAxis] = PositionMinMax.x;
                        Velocity = Vector2.zero;
                    }
                    break;
                case DirectionVariants.Vertical:
                    if (position[moveAxis] >= PositionMinMax.y)
                    {
                        position[moveAxis] = PositionMinMax.y;
                        Velocity = Vector2.zero;
                    }
                    else if (position[moveAxis] <= PositionMinMax.x)
                    {
                        position[moveAxis] = PositionMinMax.x;
                        Velocity = Vector2.zero;
                    }
                    break;
            }
        }

        #region ForAdapter
        public ViewHolder GetFreeHolder(object element)
        {
            if (_freeElement != null)
            {
                var holder = _freeElement.Value.Value;
                if (Equals(_freeElement.Value.Key, element))
                {
                    _freeElement = null;
                    return holder;
                }
                _poolElements.Add(_freeElement.Value.Key, holder);
                _freeElement = null;
            }
            
            return _poolElements.Get<ViewHolder>(element);
        }

        public ViewHolder TryGetViewHolderByPosition(int position)
        {
            return TryGetViewHolderByPosition(position, _left) ??
                    TryGetViewHolderByPosition(position, _itemsHandlers) ??
                    TryGetViewHolderByPosition(position, _right);
        }

        private static ViewHolder TryGetViewHolderByPosition(int position, LinkedList<ViewHolder> holders)
        {
            if (holders.Count == 0)
                return null;
            var node = holders.First;
            while (node != null)
            {
                if (node.Value.Position == position)
                    return node.Value;
                node = node.Next;
            }
            return null;
        }

        #endregion

        #region DragMetods

        public void OnDrag(PointerEventData eventData)
        {
            _lastEventData = eventData;
            if (eventData.button != PointerEventData.InputButton.Left || !IsActive() || !CanMove)
            {
                ExecuteEvents.ExecuteHierarchy<IDragHandler>(_parantGo, eventData, OnDragHandlerUp);
                return;
            }
            if (IsExecuteEventHandlerHierarchyUp)
                ExecuteEvents.ExecuteHierarchy<IDragHandler>(_parantGo, eventData, OnDragHandlerUp);

            NormalMove(eventData);
        }

        private void OnDragHandlerUp(IDragHandler handler, BaseEventData eventData)
        {
            handler.OnDrag(_lastEventData);
        }

        private void SetContentPosition(Vector2 position)
        {
            var moveAxis = MoveAxis;
            Vector3 modPos = Content.RectTransform.position;
            modPos[moveAxis] = position[moveAxis];

            if (modPos != Content.RectTransform.position)
                Content.RectTransform.position = modPos;
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            _lastEventData = eventData;
            if (eventData.button != PointerEventData.InputButton.Left || !IsActive() || !CanMove)
            {
                ExecuteEvents.ExecuteHierarchy<IBeginDragHandler>(_parantGo, eventData, OnBeingDrugHandlerUp);
                return;
            }
            if(IsExecuteEventHandlerHierarchyUp)
                ExecuteEvents.ExecuteHierarchy<IBeginDragHandler>(_parantGo, eventData, OnBeingDrugHandlerUp);

            //if (CurrentState == States.Elastic)
            //    return;

            _pointerStartLocalCursor = Vector2.zero;
            RectTransformUtility.ScreenPointToLocalPointInRectangle(ViewRectTransform, eventData.position, eventData.pressEventCamera, out _pointerStartLocalCursor);
            _contentStartPosition = Content.RectTransform.position;
            CurrentState = States.Drag;
            CurrnetOffset = DirOffset.None;
        }

        private void OnBeingDrugHandlerUp(IBeginDragHandler handler, BaseEventData eventData)
        {
            handler.OnBeginDrag(_lastEventData);
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            _lastEventData = eventData;
            if (eventData.button != PointerEventData.InputButton.Left || !IsActive() || !CanMove)
            {
                ExecuteEvents.ExecuteHierarchy<IEndDragHandler>(_parantGo, eventData, OnEndDrugHundlerUp);
                return;
            }
            if(IsExecuteEventHandlerHierarchyUp)
                ExecuteEvents.ExecuteHierarchy<IEndDragHandler>(_parantGo, eventData, OnEndDrugHundlerUp);

            CurrentState = States.None;
            if(MoveType == MoveProcessingVariants.Centring)
                OnEndDrug_CalculateCentring();
        }

        private void OnEndDrugHundlerUp(IEndDragHandler handler, BaseEventData eventData)
        {
            handler.OnEndDrag(_lastEventData);
        }

        protected virtual void OnEndDrug_CalculateCentring()
        {
            if (Mathf.Abs(Velocity[MoveAxis]) / transform.lossyScale[MoveAxis] <= 0.1f)
            {
                BeginCalculateCentring(true);
            }
        }
        
        public void OnInitializePotentialDrag(PointerEventData eventData)
        {
            _lastEventData = eventData;
            if (eventData.button != PointerEventData.InputButton.Left || !IsActive() || !CanMove)
            {
                //ExecuteEvents.ExecuteHierarchy<IInitializePotentialDragHandler>(_parantGo, eventData, OnInitiazizePotentialDrugHandleUp);
                return;
            }
            if (IsExecuteEventHandlerHierarchyUp)
                ExecuteEvents.ExecuteHierarchy<IInitializePotentialDragHandler>(_parantGo, eventData, OnInitiazizePotentialDrugHandleUp);

            if (!IsActive())
                return;

            Velocity = Vector2.zero;
        }

        private void OnInitiazizePotentialDrugHandleUp(IInitializePotentialDragHandler handler, BaseEventData eventData)
        {
            handler.OnInitializePotentialDrag(_lastEventData);
        }


        public override bool IsActive()
        {
            return base.IsActive() && Adapter != null && Adapter.ItemCount >= 1 && SizeContentMoreViewRect();
        }

        private bool SizeContentMoreViewRect()
        {
            int axis = MoveAxis;
            return _right.Count != 0 || _left.Count != 0 || ContentSize.size[axis] >= ViewRectTransform.rect.size[axis] * ViewRectTransform.lossyScale[axis];
        }

        #endregion
    }
}