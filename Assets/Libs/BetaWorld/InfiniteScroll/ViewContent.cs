﻿using UnityEngine;

namespace BetaWorld.InfiniteScroll
{
    public sealed class ViewContent
    {
        public readonly RectTransform RectTransform;
        private readonly RectTransform ViewPort;

        public ViewContent(RectTransform rectTransform)
        {
            RectTransform = rectTransform;
            ViewPort = (RectTransform)rectTransform.parent;
        }

        private readonly Vector3[] _corners = new Vector3[4];
        public Vector3[] CurrentCorners
        {
            get
            {
                ViewPort.GetWorldCorners(_corners);

                return _corners;
            }
        }

        
    }
}