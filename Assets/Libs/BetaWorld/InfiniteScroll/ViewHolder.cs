﻿using System;
using BetaWorld.Extensions;
using UnityEngine;
using UnityEngine.EventSystems;

namespace BetaWorld.InfiniteScroll
{
    public enum StandartActions
    {
        Click
    }

    public abstract class ViewHolder : MonoBehaviour, IPointerClickHandler, IDisposable
    {
        public object CurrentType { get; private set; }
        public VisibleObject VisibleObject { get; private set; }
		
        public virtual bool IsVariableSize { get { return false; } }

        private RectTransform _rectTransform;
        public RectTransform rectTransform
        {
            get
            {
                if (_rectTransform == null)
                    _rectTransform = (RectTransform)transform;
                return _rectTransform;
            }
        }

        public int Position { get; set; }

        private Func<Enum, Action> _bindenActions;
        private Func<Enum, Action<object[]>> _bindenActionsObjects;

        public void InitHolder(ViewContent content, object type)
        {
            CurrentType = type;
            if (VisibleObject != null)
                return;

            VisibleObject = new VisibleObject(content, rectTransform);
        }

        public void ChangePosition(Vector2 positionNew, TransformHelper.Direction direction)
        {
            var myPosDir = VisibleObject.GetCorner(direction);
            rectTransform.position = (rectTransform.position - (Vector3)myPosDir) + (Vector3)positionNew;
        }

        public void NotifyAction(Enum codeAction)
        {
            if (_bindenActions != null)
            {
                var action = _bindenActions.Invoke(codeAction);
                if(action != null)
                    action.Invoke();
            }
        }

        public void NotifyAction(Enum codeAction, params object[] paramObjects)
        {
            if (_bindenActionsObjects != null)
            {
                var action = _bindenActionsObjects.Invoke(codeAction);
                if (action != null)
                    action.Invoke(paramObjects);
            }
        }

        public virtual void OnPointerClick(PointerEventData eventData)
        {
            NotifyAction(StandartActions.Click);
        }


        internal void BindHolderActions(Func<Enum, Action> bindenActions, Func<Enum, Action<object[]>> bindenActionsObjects)
        {
            _bindenActions = bindenActions;
            _bindenActionsObjects = bindenActionsObjects;
        }

        public virtual float GetSize(object value, RectTransform viewRect, int axis)
        {
            return viewRect.rect.size[axis];
        }

        public virtual void Dispose()
        {
            
        }
    }
}
